import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AirRideLimo extends Application {

	public static void main(String[] args) {

		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		primaryStage.setTitle("Airport Ride Calculator");
		Label heading = new Label("From:");
		String locations[] = { "Brampton", "Cestar College" };
		ComboBox<String> combo_box = new ComboBox<>(FXCollections.observableArrayList(locations));
		Label selected = new Label("default item selected");

		Button btn = new Button();
		btn.setText("Calculate!");
		Label resultsLabel = new Label("");

		VBox root = new VBox();
		root.getChildren().add(heading);
		root.getChildren().add(combo_box);
		String cb[] = { "Extra luggage?", "Pets", "Use 407 ETR highway?", "Tip?" };
		for (int i = 0; i < cb.length; i++) {
			CheckBox c = new CheckBox(cb[i]);
			root.getChildren().add(c);
		}
		root.getChildren().add(btn);
		root.getChildren().add(resultsLabel);

		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				System.out.println("BUTTON PRESSED!");

				System.out.println(selected);
				String selected1 = combo_box.getValue().toString();
				if (selected1 == "Brampton") {
					selected1 = "38";
					System.out.println(selected1);
				} else {
					selected1 = "51";
					System.out.println(selected1);
				}

				double baseFare = Double.parseDouble(selected1);
				double Tip = 0;
				System.out.println(baseFare);

				if (((CheckBox) root.getChildren().get(2)).isSelected()) {
					baseFare = baseFare + 10;
				}
				if (((CheckBox) root.getChildren().get(3)).isSelected()) {
					baseFare = baseFare + 6;
				}
				if (((CheckBox) root.getChildren().get(4)).isSelected()) {
					baseFare = baseFare + 20;
				}
				double Tax = baseFare * 0.13;
				if (((CheckBox) root.getChildren().get(5)).isSelected()) {
					Tip = (baseFare + Tax) * 0.15;
				}
				double totalFare = baseFare + Tax + Tip;
				System.out.println(totalFare);
				resultsLabel.setText("The total price for your ride: $" +totalFare);

			}
		});
		root.setPadding(new Insets(10, 80, 10, 80));
		root.setSpacing(10);
		primaryStage.setScene(new Scene(root, 400, 300));
		primaryStage.show();
	}

}
